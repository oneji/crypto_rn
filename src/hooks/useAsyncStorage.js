import { useState, useEffect } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { G } from 'react-native-svg';

// Hook to get data from AsyncStorage
const useAsyncStorage = (key) => {
    const [data, setData] = useState('');

    useEffect(() => {
        const fetchData = async () => {
            try {
                let nonJsonData = await AsyncStorage.getItem(key);

                console.log(nonJsonData)
    
                return nonJsonData != null ? setData(JSON.parse(nonJsonData)) : null;
            } catch (error) {
                console.log('AsyncStorage error...', error);
            }
        }

        fetchData();
    }, [])

    return data;
}

export default useAsyncStorage;
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from '../axios'

class AuthService {
    /**
     * Login user by credentials
     * 
     * @param {phone_number, password} credentials 
     */
    static login = credentials => {
        return axios.post('auth/login', credentials);
    }

    /**
     * Register user by credentials
     * 
     * @param {phone_number, password} credentials 
     */
    static register = credentials => {
        return axios.post('auth/register', credentials);
    }

    /**
     * Verify code
     * 
     * @param {phone_number, verify_code} credentials
     */
    static verify = credentials => {
        return axios.put('auth/verify', credentials);
    }

    /**
     * Complete register
     * 
     * @param {phone_number, email, pin_code} credentials
     */
    static completeRegister = credentials => {
        return axios.put('auth/completeRegister', credentials);
    }

    /**
     * Store JWT token in AsyncStorage 
     * 
     * @param {string} token
     */
    static storeToken = async token => {
        try {
            await AsyncStorage.setItem('@crypto_token', JSON.stringify(token));
        } catch (error) {
            console.log('AsyncStorage error', error)
        }
    }

    /**
     * Store user object in AsyncStorage
     * 
     * @param {object} user
     */
    static storeUserObject = async user => {
        try {
            await AsyncStorage.setItem('@crypto_user', JSON.stringify(user));
        } catch (error) {
            console.log('AsyncStorage error', error)
        }
    }

    /**
     * Retrieve token and user object from AsyncStorage
     */
    static getUserCredentials = async () => {
        try {
            let user = await AsyncStorage.getItem('@crypto_user')
            let token = await AsyncStorage.getItem('@crypto_token')
            let data = {
                user: null,
                token: null
            };

            if(user) data.user = JSON.parse(user);
            if(token) data.token = JSON.parse(token);

            return data;
        } catch (error) {
            console.log('AsyncStorage error', error)
        }
    }

    /**
     * Logout
     */
    static logout = async () => {
        try {
            console.log('...')
            await AsyncStorage.multiRemove(['@crypto_user', '@crypto_token']);
        } catch (error) {
            console.log('AsyncStorage error', error)
        }
    }

    /**
     * Restore user password
     * 
     * @param {string} phoneNumber 
     */
    static restorePassword = phoneNumber => {
        return axios.post('auth/restorePassword', {
            phone_number: phoneNumber
        });
    }

    /**
     * Restore verify code
     * 
     * @param {phoneNumber, code} credentials
     */
    static verifyRestore = ({ phoneNumber, code }) => {
        return axios.put('auth/verifyRestorePassword', {
            phone_number: phoneNumber,
            verify_restore_code: code
        });
    }

    /**
     * Update password
     * 
     * @param {phoneNumber, password} credentials
     */
    static updatePassword = ({ phoneNumber, password }) => {
        return axios.put('auth/generateNewPassword', {
            phone_number: phoneNumber,
            new_password: password
        });
    }

    /**
     * Resend verification code
     * 
     * @param {string} phone_number 
     */
    static resendVerificationCode = phoneNumber => {
        return axios.put('auth/resendVerificationCode', {
            phone_number: phoneNumber
        });
    }
}

export default AuthService
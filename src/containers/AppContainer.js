import React from 'react'
import { SafeAreaView, StatusBar } from 'react-native';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { colors } from '../styles/globalStyles';
// Auth and App stacks
import AuthStack from '../navigation/AuthStack';
import AppStack from '../navigation/AppStack'

// Navigation
import { navigationRef } from '../navigation/RootNavigation';
import { useDispatch, useSelector } from 'react-redux';

import { Spinner } from '../components/ui'

// Custom hooks
import { useAsyncStorage } from '../hooks'
import { loginSuccess } from '../redux/actions/login'
import { AuthService } from '../services';

const Stack = createStackNavigator();
const MyTheme = {
    ...DefaultTheme,
    colors: {
        background: colors.darkBg,
        text: colors.white
    }
}

const AppContainer = () => {
    const [loading, setLoading] = React.useState(true);
    const dispatch = useDispatch();

    const { isAuthenticated } = useSelector(state => state.user);

    React.useEffect(() => {
        const fetchCredentials = async () => {
            try {
                setLoading(true);
                let { user, token } = await AuthService.getUserCredentials();
                
                if(!isAuthenticated && token) {
                    dispatch(loginSuccess({ token, user }));
                }
                
                setLoading(false)
            } catch (error) {
                setLoading(false)
            }
        }

        fetchCredentials();
    }, [])

    if(loading) {
        return <Spinner />
    }

    return (
        <NavigationContainer theme={MyTheme} ref={navigationRef}>
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: colors.darkBg,
                }}
            >
                <StatusBar backgroundColor={colors.darkBg} />
                <Stack.Navigator headerMode="none">
                    {!isAuthenticated ? (
                        // Auth stack
                        <Stack.Screen
                            name="AuthStack"
                            component={AuthStack}
                        />
                    ) : (
                        // AppStack
                        <Stack.Screen
                            name="AppStack"
                            component={AppStack}
                        />
                    )}
                </Stack.Navigator>
            </SafeAreaView>
        </NavigationContainer>
    )
}

export default AppContainer

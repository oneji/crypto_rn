import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { colors } from '../styles/globalStyles'
import * as screens from '../screens/app/Deposit'
import HeaderButton from '../components/Buttons/HeaderButton';

const Stack = createStackNavigator();

const DepositScreenStack = ({ navigation }) => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: colors.dark,
                },
                headerTintColor: colors.white,
                headerTitleStyle: {
                    fontFamily: 'SFProText-Regular',
                    fontSize: 18
                },
                headerTitleAlign: 'center',
                headerLeft: () => (
                    <HeaderButton
                        icon="arrowLeft"
                        color={colors.white}
                        onPress={() => navigation.goBack()}
                    />
                ),
            }}
        >
            <Stack.Screen
                name="DepositScreen"
                component={screens.DepositScreen}
                options={{
                    title: 'Вклад',
                    headerLeft: null
                }}
            />
            
            <Stack.Screen
                name="DepositChooseCryptoScreen"
                component={screens.ChooseCryptoScreen}
                options={{
                    title: 'Вклад'
                }}
            />
            
            <Stack.Screen
                name="ChooseTermScreen"
                component={screens.ChooseTermScreen}
                options={{
                    title: 'Вклад'
                }}
            />
        </Stack.Navigator>
    )
}

export default DepositScreenStack
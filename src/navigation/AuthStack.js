import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import * as screens from '../screens'
import { colors } from '../styles/globalStyles'
import HeaderButton from '../components/Buttons/HeaderButton';

const Stack = createStackNavigator();

const AuthStack = ({ navigation }) => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerLeft: () => (
                    <HeaderButton
                        icon="arrowLeft"
                        color={colors.white}
                        onPress={() => navigation.goBack()}
                    />
                ),
                headerStyle: {
                    backgroundColor: colors.dark,
                },
                headerTintColor: colors.white,
                headerTitleStyle: {
                    fontFamily: 'SFProText-Regular',
                    fontSize: 18
                },
                headerTitleAlign: 'center',
            }}
        >
            <Stack.Screen
                name="HelloScreen"
                component={screens.HelloScreen}
                options={{
                    headerShown: false
                }}
            />

            <Stack.Screen
                name="RegisterScreen"
                component={screens.RegisterScreen}
                options={{ title: 'Регистрация' }}
            />
            
            <Stack.Screen
                name="EnterSmsScreen"
                component={screens.EnterSmsScreen}
                options={{ title: 'Сброс пароля' }}
            />
            
            <Stack.Screen
                name="EnterEmailScreen"
                component={screens.EnterEmailScreen}
                options={{ title: 'Электронный адрес' }}
            />
            
            <Stack.Screen
                name="PinScreen"
                component={screens.PinScreen}
                options={{ title: 'Создайте PIN' }}
            />
            
            <Stack.Screen
                name="WizardScreen"
                component={screens.WizardScreen}
            />

            <Stack.Screen
                name="StartScreen"
                component={screens.StartScreen}
                options={{
                    title: 'Успех',
                    headerLeft: null
                }}
            />
            
            <Stack.Screen
                name="LoginScreen"
                component={screens.LoginScreen}
                options={{ title: 'Войти в кабинет' }}
            />
            
            <Stack.Screen
                name="ForgotPasswordScreen"
                component={screens.ForgotPasswordScreen}
                options={{ title: 'Сброс пароля' }}
            />
            
            <Stack.Screen
                name="NewPasswordScreen"
                component={screens.NewPasswordScreen}
                options={{ title: 'Сброс пароля' }}
            />
            
            <Stack.Screen
                name="RestoreSuccessScreen"
                component={screens.RestoreSuccessScreen}
                options={{ title: 'Успех' }}
            />
        </Stack.Navigator>
    )
}

export default AuthStack
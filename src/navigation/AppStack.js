import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as screens from '../screens/app'
import { colors } from '../styles/globalStyles';
import Icon from '../components/ui/Icon';

import HomeScreenStack from './HomeScreenStack'
import DepositScreenStack from './DepositScreenStack'
import CreditScreenStack from './CreditScreenStack'
import CardScreenStack from './CardScreenStack'

const Tab = createBottomTabNavigator();

const AppStack = () => {
    return (
        <Tab.Navigator
            initialRouteName="HomeScreen"
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    const icons = {
                        HomeScreen: 'home',
                        DepositScreen: 'wallet',
                        CreditScreen: 'credit',
                        CardScreen: 'card',
                        ProfileScreen: 'user'
                    }
                    
                    return <Icon type={icons[route.name]} color={color} />
                },
            })}
            tabBarOptions={{
                activeTintColor: colors.green,
                inactiveTintColor: colors.grayText,
                tabStyle: {
                    padding: 3
                },
                style: {
                    height: 60,
                    backgroundColor: colors.darkBg,
                    opacity: 3
                },
                labelStyle: {
                    fontSize: 12
                }
            }}
        >
            <Tab.Screen
                name="HomeScreen"
                component={HomeScreenStack}
                options={{
                    tabBarLabel: 'Главная'
                }}
            />

            <Tab.Screen
                name="DepositScreen"
                component={DepositScreenStack}
                options={{
                    tabBarLabel: 'Вклад',
                    tabBarVisible: false
                }}
            />

            <Tab.Screen
                name="CreditScreen"
                component={CreditScreenStack}
                options={{
                    tabBarLabel: 'Кредит'
                }}
            />
            
            <Tab.Screen
                name="CardScreen"
                component={CardScreenStack}
                options={{
                    tabBarLabel: 'Карта'
                }}
            />
            
            <Tab.Screen
                name="ProfileScreen"
                component={screens.ProfileScreen}
                options={{
                    tabBarLabel: 'Профиль'
                }}
            />
        </Tab.Navigator>
    )
}

export default AppStack;
import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { colors } from '../styles/globalStyles'
import * as screens from '../screens/app'
import HeaderButton from '../components/Buttons/HeaderButton';

const Stack = createStackNavigator();

import { logout } from '../redux/actions/login'
import { useDispatch } from 'react-redux';

const HomeScreenStack = ({ navigation }) => {
    const dispatch = useDispatch();

    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: colors.dark,
                },
                headerTintColor: colors.white,
                headerTitleStyle: {
                    fontFamily: 'SFProText-Regular',
                    fontSize: 18
                },
                headerTitleAlign: 'center',
            }}
        >
            <Stack.Screen
                name="HomeScreen"
                component={screens.HomeScreen}
                options={{
                    title: 'Главная',
                    headerLeft: () => (
                        <HeaderButton
                            icon="gear"
                            color={colors.white}
                            onPress={() => dispatch(logout())}
                        />
                    ),
                    headerRight: () => (
                        <HeaderButton
                            icon="bell"
                            color={colors.white}
                            onPress={() => {}}
                        />
                    ),
                }}
            />
        </Stack.Navigator>
    )
}

export default HomeScreenStack
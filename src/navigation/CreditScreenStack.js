import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { colors } from '../styles/globalStyles'
import * as screens from '../screens/app/Credit'
import HeaderButton from '../components/Buttons/HeaderButton';

const Stack = createStackNavigator();

const CreditScreenStack = ({ navigation }) => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: colors.dark,
                },
                headerTintColor: colors.white,
                headerTitleStyle: {
                    fontFamily: 'SFProText-Regular',
                    fontSize: 18
                },
                headerTitleAlign: 'center',
                headerLeft: () => (
                    <HeaderButton
                        icon="arrowLeft"
                        color={colors.white}
                        onPress={() => navigation.goBack()}
                    />
                ),
            }}
        >
            <Stack.Screen
                name="CreditScreen"
                component={screens.CreditScreen}
                options={{
                    title: 'Кредит',
                    headerLeft: null,
                }}
            />
            
            <Stack.Screen
                name="CreditChooseCryptoScreen"
                component={screens.ChooseCryptoScreen}
                options={{
                    title: 'Кредит',
                    tabBarVisible: false
                }}
            />
            
            <Stack.Screen
                name="CalculateScreen"
                component={screens.CalculateScreen}
                options={({ route }) => ({
                    title: `Расчет ${route.params.currency} залога`
                })}
            />
        </Stack.Navigator>
    )
}

export default CreditScreenStack
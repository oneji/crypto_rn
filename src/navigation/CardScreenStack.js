import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { colors } from '../styles/globalStyles'
import * as screens from '../screens/app/Card'
import HeaderButton from '../components/Buttons/HeaderButton';

const Stack = createStackNavigator();

const CardScreenStack = ({ navigation }) => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: colors.dark,
                },
                headerTintColor: colors.white,
                headerTitleStyle: {
                    fontFamily: 'SFProText-Regular',
                    fontSize: 18
                },
                headerTitleAlign: 'center',
                headerLeft: () => (
                    <HeaderButton
                        icon="arrowLeft"
                        color={colors.white}
                        onPress={() => navigation.goBack()}
                    />
                ),
            }}
        >
            <Stack.Screen
                name="CardScreen"
                component={screens.CardScreen}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    )
}

export default CardScreenStack
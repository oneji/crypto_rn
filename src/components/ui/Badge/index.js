import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { colors } from '../../../styles/globalStyles'

const setBagdeColor = (type) => {
    switch (type) {
        case 'success':
            return colors.green
        case 'danger':
            return colors.red
    
        default:
            return colors.white;
    }
}

const Badge = ({ type, children }) => {
    return (
        <Text
            style={{
                color: setBagdeColor(type),
                textAlign: 'center',
                backgroundColor: colors.grayBadge,
                borderRadius: 5,
                fontFamily: 'SFProText-Medium',
                fontSize: 12,
                paddingVertical: 2,
                paddingHorizontal: 5
            }}
        >
            {children}
         </Text>
    )
}

export default Badge
import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { colors } from '../../../styles/globalStyles'
import Icon from '../Icon'

const Checkbox = ({ size = 24, circle, label }) => {
    const [active, setActive] = useState(false);

    return (
        <TouchableOpacity
            style={styles.container}
            onPress={() => setActive(prevActive => !prevActive)}
            activeOpacity={1}
        >
            <>
            <View
                style={[
                    styles.checkbox,
                    {
                        width: size,
                        height: size,
                        borderRadius: circle  ? 100 : 2,
                        backgroundColor: circle || active ? colors.green : null,
                        borderWidth: circle ? 0 : 1, 
                        borderColor: circle || active ? null : colors.gray
                    }
                ]}
            >
                {active ? (<Icon type="check" />) : null}
                
            </View>
            <Text style={styles.label}>{label}</Text>
            </>
        </TouchableOpacity>
    )
}

export default Checkbox

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginBottom: 16
    },
    checkbox: {
        marginRight: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        fontSize: 14,
        fontFamily: 'SFProText-Regular',
        color: colors.grayText,
        lineHeight: 20,
        flexShrink: 1
    }
})

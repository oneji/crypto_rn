import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { colors } from '../../../styles/globalStyles'
import Icon from '../Icon'

const IconButton = ({ icon = 'download', children, onPress }) => {
    return (
        <TouchableOpacity style={styles.btn} onPress={onPress}>
            <Icon
                type={icon}
                color={colors.white}
            />
            <Text style={styles.btnText}>{children}</Text>
        </TouchableOpacity>
    )
}

export default IconButton

const styles = StyleSheet.create({
    btn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
        borderColor: colors.grayText,
        borderWidth: 1,
        borderRadius: 15,
        backgroundColor: '#2D2D2D',
        margin: 5,
        paddingHorizontal: 7,
        minWidth: 80
    },
    btnText: {
        fontSize: 12,
        color: colors.white,
        fontFamily: 'SFProText-Regular',
        marginTop: 4
    }
})

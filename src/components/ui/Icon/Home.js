import * as React from "react"
import Svg, { Path } from "react-native-svg"

const Home = (props) => {
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            viewBox="0 0 24 24"
            {...props}
        >
            <Path fill="none" d="M0 0h24v24H0z" />
            <Path
                d="M14.762 23a.644.644 0 01-.644-.644v-5.179a1.083 1.083 0 00-1.082-1.077h-2.071a1.083 1.083 0 00-1.082 1.081v5.178a.644.644 0 01-.644.641H5.727a2.373 2.373 0 01-2.371-2.37v-6.6h-.419a2.025 2.025 0 01-1.348-3.458l.011-.009 8.968-8.97a2.026 2.026 0 012.865 0l8.975 8.974a2.026 2.026 0 01-1.434 3.457h-.331v6.6A2.373 2.373 0 0118.273 23z"
                fill={props.color}
            />
        </Svg>
    )
}

export default Home
import * as React from "react"
import Svg, { Path } from "react-native-svg"

function ArrowLeft(props) {
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={10.4}
            height={17}
            viewBox="0 0 10.4 17"
            {...props}
        >
            <Path
                d="M3.659 8.5l5.985-5.982a.877.877 0 000-1.238L9.12.756a.877.877 0 00-1.238 0L.755 7.882a.883.883 0 000 1.242l7.12 7.12a.877.877 0 001.238 0l.524-.524a.876.876 0 000-1.238z"
                fill="#f0f7f9"
                stroke="#15204b"
                strokeMiterlimit={10}
            />
        </Svg>
    )
}

export default ArrowLeft

import * as React from "react"
import Svg, { Path } from "react-native-svg"

const Check = ({ color = '#121212' }) => {
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={11.75}
            height={8.5}
            viewBox="0 0 11.75 8.5"
        >
            <Path
                d="M11.457 1.707l-6.5 6.5a1 1 0 01-1.414 0l-3.25-3.25a1 1 0 011.414-1.414L4.25 6.086 10.043.293a1 1 0 011.414 1.414z"
                fill={color}
            />
        </Svg>
    )
}

export default Check

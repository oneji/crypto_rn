import * as React from "react"
import Svg, { Path } from "react-native-svg"

const ArrowRight = ({ color = '#FFFFFF' }) => {
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={9}
            height={14}
            viewBox="0 0 9 14"
        >
            <Path
                d="M5.812 7L.717 2.139A.689.689 0 01.5 1.637a.69.69 0 01.217-.5l.446-.426A.757.757 0 011.691.5a.757.757 0 01.527.208L8.283 6.5A.689.689 0 018.5 7a.689.689 0 01-.217.5l-6.06 5.785a.757.757 0 01-.523.215.757.757 0 01-.527-.208l-.446-.426a.689.689 0 010-1.006z"
                fill={color}
                stroke="#15204b"
                strokeMiterlimit={10}
                data-name="ic/arrow/left"
            />
        </Svg>
    )
}

export default ArrowRight

import * as React from "react"
import Svg, { Path } from "react-native-svg"

const Redo = ({ size = 15.36, color }) => {
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={15.36}
            height={15.36}
            viewBox={`0 0 ${size} ${size}`}
        >
            <Path fill="none" d="M0 0h15.36v15.36H0z" />
            <Path
                d="M10.128 14.08a.55.55 0 110-1.1h1.812a.55.55 0 110 1.1zm-4.676 0a4.8 4.8 0 110-9.6h7.858l-2.937-2.901a.55.55 0 01.774-.78l3.088 3.051a1.643 1.643 0 010 2.329l-3.057 3.076a.55.55 0 01-.781-.772l2.889-2.905H5.452a3.703 3.703 0 100 7.405h1.9a.55.55 0 110 1.1z"
                fill={color}
                stroke={color}
                strokeMiterlimit={10}
                strokeWidth={0.1}
            />
        </Svg>
    )
}

export default Redo

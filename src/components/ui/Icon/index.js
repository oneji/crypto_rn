import React from 'react';
import Eye from './Eye'
import ArrowLeft from './ArrowLeft'
import Home from './Home'
import Wallet from './Wallet'
import Credit from './Credit'
import Card from './Card'
import User from './User'
import Gear from './Gear'
import Bell from './Bell'
import Download from './Download'
import ArrowTop from './ArrowTop'
import Redo from './Redo'
import Transfer from './Transfer'
import Withdraw from './Withdraw'
import Check from './Check'
import ArrowRight from './ArrowRight';

const icons = {
    eye: Eye,
    arrowLeft: ArrowLeft,
    home: Home,
    wallet: Wallet,
    credit: Credit,
    card: Card,
    user: User,
    gear: Gear,
    bell: Bell,
    download: Download,
    arrowTop: ArrowTop,
    redo: Redo,
    transfer: Transfer,
    withdraw: Withdraw,
    check: Check,
    arrowRight: ArrowRight
};

export default (props) => {
    const Icon = icons[props.type];
    return <Icon {...props} />;
};

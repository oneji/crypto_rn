import * as React from "react"
import Svg, { Path } from "react-native-svg"

const Bell = (props) => {
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={19.847}
            height={23.816}
            viewBox="0 0 19.847 23.816"
            {...props}
        >
            <Path
                d="M11.412 4.148a.5.5 0 01-.5-.5V1.985a.992.992 0 00-1.985 0v1.667a.5.5 0 11-.992 0V1.985a1.985 1.985 0 013.969 0v1.667a.5.5 0 01-.492.496z"
                fill={props.color}
            />
            <Path
                data-name="Path"
                d="M9.923 23.816a3.477 3.477 0 01-3.473-3.469.5.5 0 01.992 0 2.481 2.481 0 004.962 0 .5.5 0 11.992 0 3.477 3.477 0 01-3.473 3.469z"
                fill={props.color}
            />
            <Path
                d="M1.489 20.839a1.489 1.489 0 01-.968-2.62 6.9 6.9 0 002.456-5.281V9.923a6.946 6.946 0 1113.892 0v3.015a6.891 6.891 0 002.448 5.274 1.489 1.489 0 01-.959 2.627z"
                fill={props.color}
            />
        </Svg>
    )
}

export default Bell

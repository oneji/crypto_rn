import * as React from "react"
import Svg, { Path, G } from "react-native-svg"

const Withdraw = ({ size = 16.2, color }) => {
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={size}
            height={size}
            viewBox={`0 0 ${size} ${size}`}
        >
            <G
                fill={color}
                stroke={color}
                strokeMiterlimit={10}
                strokeWidth={0.2}
            >
                <Path d="M10.139 12.825l-1.484 1.236V9.569a.314.314 0 00-.626 0v4.492l-1.485-1.236a.314.314 0 00-.4.483l2 1.666h0a.314.314 0 00.053.035h0a.314.314 0 00.119.028h0a.314.314 0 00.113-.025l.035-.019a.314.314 0 00.047-.028l2-1.666a.314.314 0 00-.372-.474z" />
                <Path
                    data-name="Path"
                    d="M2.61 3.865H1.041a.314.314 0 01-.314-.314V.414a.314.314 0 00-.627 0v3.162a.941.941 0 00.941.941H2.61a.314.314 0 100-.627zM15.786.1a.314.314 0 00-.31.314v3.162a.314.314 0 01-.314.314H13.59a.314.314 0 100 .627h1.568a.941.941 0 00.941-.941V.414A.314.314 0 0015.786.1z"
                />
                <Path d="M11.202 8.757H5.496a1.858 1.858 0 01-1.855-1.852V2.296H2.61a.314.314 0 110-.627h10.98a.314.314 0 110 .627l-.536.013a.276.276 0 010 .038v4.558a1.858 1.858 0 01-1.852 1.852zM4.264 2.309h0v4.596a1.228 1.228 0 00.364.87 1.214 1.214 0 00.859.353h5.709a1.225 1.225 0 001.224-1.223V2.347a.257.257 0 01.022-.038h-.765a2.7 2.7 0 01-.314.009 2.835 2.835 0 01-.314-.009h-.738a2.7 2.7 0 01-.315.009 2.83 2.83 0 01-.312-.009H5.673z" />
            </G>
        </Svg>
    )
}

export default Withdraw

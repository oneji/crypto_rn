import * as React from "react"
import Svg, { Path } from "react-native-svg"

const Card = ({ color, size = 26 }) => {
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={size}
            height={size}
            viewBox={`0 0 ${size} ${size}`}
        >
            <Path fill="none" d="M0 0h26v26H0z" />
            <Path
                data-name="Combined Shape"
                d="M3.689 21.667a2.6 2.6 0 01-2.606-2.585V8.002a2.6 2.6 0 012.606-2.585h18.62a2.6 2.6 0 012.606 2.585v11.08a2.6 2.6 0 01-2.606 2.586zm-1.116-2.585A1.114 1.114 0 003.69 20.19h18.62a1.114 1.114 0 001.117-1.108v-8.125H2.573zm0-11.08V9.48h20.853V8.002a1.114 1.114 0 00-1.117-1.108H3.689a1.114 1.114 0 00-1.116 1.108zm2.979 9.972a.741.741 0 01-.744-.739v-.738a.741.741 0 01.744-.739h.745a.741.741 0 01.744.739v.738a.741.741 0 01-.744.739z"
                fill={color}
            />
        </Svg>
    )
}

export default Card

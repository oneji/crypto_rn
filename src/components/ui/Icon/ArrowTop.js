import * as React from "react"
import Svg, { Path, G } from "react-native-svg"

const ArrowTop = ({ size = 16, color }) => {
    return (
        <Svg
            data-name="Group 15"
            xmlns="http://www.w3.org/2000/svg"
            width={size}
            height={size}
            viewBox={`0 0 ${size} ${size}`}
        >
        <Path fill="none" d="M0 0h16v16H0z" />
        <G data-name="Group 16" fill={color}>
            <Path d="M7.717 4.283a.4.4 0 01.566 0l3.376 3.376a.2.2 0 01-.142.341H4.483a.2.2 0 01-.141-.341z" />
            <Path
                data-name="Rectangle"
                d="M7.5 7h1v5.7a.3.3 0 01-.3.3h-.4a.3.3 0 01-.3-.3V7z"
            />
        </G>
        </Svg>
    )
}

export default ArrowTop

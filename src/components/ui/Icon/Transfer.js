import * as React from "react"
import Svg, { Path, G } from "react-native-svg"

const Transfer = ({ size = 15.36, color }) => {
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={size}
            height={size}
            viewBox={`0 0 ${size} ${size}`}
        >
            <Path
                d="M2.857 15.147L.16 12.37a.544.544 0 01.779-.758l1.79 1.844V.545a.545.545 0 011.09 0v12.911l1.791-1.844a.544.544 0 01.779.758l-2.7 2.777a.542.542 0 01-.836 0zM11.4 14.8V1.887L9.6 3.731a.543.543 0 01-.778-.758L11.523.2a.543.543 0 01.836 0l2.7 2.777a.544.544 0 01-.779.758l-1.79-1.844V14.8a.545.545 0 01-1.09 0z"
                fill={color}
            />
        </Svg>
    )
}

export default Transfer

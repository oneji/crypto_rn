import * as React from "react"
import Svg, { Path } from "react-native-svg"

const Download = ({ color, size = 16 }) => {
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={size}
            height={size}
            viewBox={`0 0 ${size} ${size}`}
        >
            <Path fill="none" d={`M0 0h${size}v${size}H0z`} />
            <Path
                data-name="Combined Shape"
                d="M2.757 14.72a1.493 1.493 0 01-1.477-1.509v-3.017a.5.5 0 01.492-.5.5.5 0 01.492.5v3.017a.5.5 0 00.492.5h9.846a.5.5 0 00.492-.5v-3.017a.492.492 0 11.985 0v3.017a1.492 1.492 0 01-1.477 1.508zm4.575-4.08L4.87 8.127a.512.512 0 010-.7.485.485 0 01.7-.013l1.618 1.658V1.14a.492.492 0 11.985 0v7.932l1.622-1.656a.485.485 0 01.684 0 .511.511 0 01.012.711L8.028 10.64a.485.485 0 01-.7 0z"
                fill={color}
            />
        </Svg>
    )
}

export default Download

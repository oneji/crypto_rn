import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { colors } from '../../../styles/globalStyles'

const RadioButton = ({ 
    size = 24, 
    type = 'circle',
    label,
    style,
    description,
    labelStyle,
    descriptionStyle
}) => {
    const [active, setActive] = useState(false);

    return (
        <TouchableOpacity
            style={[
                styles.container,
                style,
                {
                    alignItems: description ? 'flex-start' : 'center'
                }
            ]}
            onPress={() => setActive(prevActive => !prevActive)}
        >
            <View
                style={[
                    styles.radio,
                    {
                        width: size,
                        height: size,
                        borderRadius: type === 'circle' ? 100 : null,
                        borderColor: active ? colors.green : '#979797'
                    }
                ]}
            >
                {active ? (
                    <View
                        style={{
                            backgroundColor: colors.green,
                            borderRadius: 100,
                            width: '100%',
                            height: '100%'
                        }}
                    ></View>
                ) : null}
            </View>
            <View style={styles.content}>
                <Text
                    style={[
                        styles.label,
                        labelStyle
                    ]}
                >
                    {label}
                </Text>
                {description ? (
                    <Text
                        style={[
                            styles.description,
                            descriptionStyle
                        ]}
                    >
                        {description}
                    </Text>
                ) : null}
            </View>
        </TouchableOpacity>
    )
}

export default RadioButton

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginBottom: 16,
    },
    radio: {
        borderWidth: 1,
        marginRight: 16,
        padding: 1
    },
    content: {
        flexShrink: 1
    },
    label: {
        fontSize: 14,
        fontFamily: 'SFProText-Regular',
        color: colors.grayText,
        lineHeight: 16
    },
    description: {
        fontSize: 14,
        color: colors.grayText,
        marginTop: 6,
        lineHeight: 20,
    }
})

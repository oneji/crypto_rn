import React from 'react'
import { StyleSheet, View } from 'react-native'
import { colors } from '../../../styles/globalStyles'

const WizardIndicator = ({ active, passed = [] }) => {
    return (
        <View style={styles.container}>
            <View
                style={[
                    styles.step,
                    {
                        backgroundColor: active === 0 ? colors.white : 0 < active ? colors.green : colors.gray,
                    }
                ]} 
            ></View>
            <View
                style={[
                    styles.step,
                    {
                        backgroundColor: active === 1 ? colors.white : 1 < active ? colors.green : colors.gray,
                    }
                ]} 
            ></View>
            <View
                style={[
                    styles.step,
                    {
                        backgroundColor: active === 2 ? colors.white : 2 < active ? colors.green : colors.gray,

                    }
                ]} 
            ></View>
        </View>
    )
}

export default WizardIndicator

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginTop: 20,
        justifyContent: 'center',
        flexDirection: 'row',
    },
    step: {
        height: 4,
        backgroundColor: colors.gray,
        width: 40,
        marginHorizontal: 16,
        borderRadius: 10
    },
})

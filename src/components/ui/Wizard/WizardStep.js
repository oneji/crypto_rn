import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import Button from '../Button'

const WizardStep = ({ children, nextStep, ...rest }) => {
    return (
        <View {...rest} style={styles.wizardStep}>
            {React.cloneElement(children, { nextStep: nextStep })}
        </View>
    )
}

export default WizardStep

const styles = StyleSheet.create({
    wizardStep: {
        flex: 1
    }
})
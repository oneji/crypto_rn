import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { StyleSheet, View } from 'react-native'
import WizardStep from './WizardStep'
import WizardIndicator from './WizardIndicator'

import { verify } from '../../../redux/actions/register'

const Wizard = ({ children, onFinish, onTitleChange }) => {
    const { step } = useSelector(state => state.wizard);
    const dispatch = useDispatch();

    useEffect(() => {
        let title = null;

        switch (step) {
            case 0:
                title = 'Подтверждение через SMS';
                break;
            case 1:
                title = 'Электронный адрес';            
                break;
            case 2:
                title = 'Создайте PIN';            
                break;
        
            default:
                break;
        }

        onTitleChange(title);
    }, [step])

    const _nextStep = (data) => {
        if(step < children.length - 1) {
            if(step === 0) {
                // Verification logic...
                dispatch(verify(data));
            }
        } else {
            onFinish(data);
        }
    }

    return (
        <View style={styles.container}>
            <View style={{ flex: 1 }}>
                {children.map((el, idx) => {
                    if(idx === step) {
                        return React.cloneElement(el, { 
                            key: idx,
                            nextStep: _nextStep
                        });
                    }
                })}
            </View>

            <WizardIndicator active={step} />
        </View>
    )
}

Wizard.Step = WizardStep;

export default Wizard

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 20
    }
})

import React from 'react'
import { StyleSheet, Text, TouchableOpacity, ActivityIndicator, Dimensions } from 'react-native'
import { colors } from '../../../styles/globalStyles'

const Button = ({ onPress, text, type, style, loading, disabled }) => {
    const setButtonColor = () => {
        if(disabled) {
            return colors.disabledGreenBtn;
        }

        switch (type) {
            case 'success':
                return colors.green
            default:
                return colors.dark;
        }
    }

    const setTextColor = () => {
        switch (type) {
            case 'success':
                return colors.dark
            default:
                return colors.white;
        }
    }

    return (
        <TouchableOpacity
            onPress={onPress}
            style={[
                styles.btn,
                style,
                {
                    backgroundColor: setButtonColor()
                }
            ]}
            disabled={disabled}
        >
            {
                !loading ? 
                <Text
                    style={{
                        ...styles.btnText,
                        color: setTextColor(),
                    }}
                >
                    {text}
                </Text>
                : <ActivityIndicator size="small" color={colors.dark} />
            }
        </TouchableOpacity>
    )
}

export default Button

const styles = StyleSheet.create({
    btn: {
        borderRadius: 15,
        paddingVertical: 11,
        width: '100%'
    },
    btnText: {
        textAlign: 'center',
        fontSize: 18
    }
})

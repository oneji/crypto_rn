import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { colors } from '../../../../styles/globalStyles'

const HintLabel = (props) => {
    return (
        <View {...props}>
            <Text style={styles.hintText}>{props.children}</Text>
        </View>
    )
}

export default HintLabel

const styles = StyleSheet.create({
    hintText: {
        color: colors.grayText,
        textAlign: 'center',
        fontSize: 14,
        paddingHorizontal: 5,
        lineHeight: 20,
        fontFamily: 'SFProText-Regular',
        lineHeight: 20,
        marginBottom: 14,
    }
})

import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { colors } from '../../../../styles/globalStyles'

const MainLabel = (props) => {
    return (
        <View {...props}>
            <Text style={styles.title}>{props.children}</Text>
        </View>
    )
}

export default MainLabel

const styles = StyleSheet.create({
    title: {
        color: colors.white,
        fontSize: 20,
        fontFamily: 'SFProText-Medium',
        textAlign: 'center',
        marginBottom: 13
    },
})

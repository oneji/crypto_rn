import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { colors } from '../../../../styles/globalStyles'

const TouchableLabel = (props) => {
    return (
        <TouchableOpacity {...props}>
            <Text style={styles.text}>{props.children}</Text>
        </TouchableOpacity>
    )
}

export default TouchableLabel

const styles = StyleSheet.create({
    text: {
        fontSize: 14,
        textDecorationLine: 'underline',
        textAlign: 'center',
        color: colors.white
    }
})

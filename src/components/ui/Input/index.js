import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput } from 'react-native'
import { colors } from '../../../styles/globalStyles'
import TextInputMask from 'react-native-text-input-mask';
import ShowPasswordButton from '../../Buttons/ShowPasswordButton'

const Input = (props) => {
    const [showPassword, setShowPassword] = useState(false)

    return (
        <View style={{ marginBottom: 16 }}>
            <Text style={styles.label}>{props.label}</Text>
            <View
                style={[
                    styles.inputWrapper,
                    {
                        borderColor: props.error ? colors.red : colors.gray
                    }
                ]}
            >
                {
                    props.type === 'phone' ? 
                    <TextInputMask
                        {...props}
                        style={styles.input}
                        placeholderTextColor={colors.white}
                        onChangeText={(formatted, extracted)  => props.onChangeText(extracted)}
                        mask={"+7 [000] [000] [00] [00]"}
                    />
                    :
                    <TextInput
                        {...props}
                        onChangeText={props.onChangeText}
                        style={styles.input}
                        placeholderTextColor={colors.white}
                        secureTextEntry={props.type === 'password' && !showPassword ? true : false}
                    />
                }
                {
                    props.type === 'password' && 
                    props.value.length > 0 ? (
                        <ShowPasswordButton
                            onPress={() => setShowPassword(!showPassword)}
                        /> 
                    ) : null
                }
            </View>
            {
                props.error ? 
                <Text style={styles.error}>{props.error}</Text>
                : null
            }
        </View>
    )
}

export default Input

const styles = StyleSheet.create({
    label: {
        color: colors.grayText,
        marginBottom: 5,
        fontFamily: 'SFProText-Regular'
    },
    inputWrapper: {
        borderRadius: 15,
        borderWidth: 2,
        paddingHorizontal: 15,
        paddingVertical: 5,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    input: {
        color: colors.white,
        fontSize: 16,
        fontFamily: 'SFProText-Regular',
        flexGrow: 1
    },
    error: {
        color: colors.red,
        fontSize: 12,
        marginTop: 8
    }
})

import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { StyleSheet, Text, View } from 'react-native'
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { colors } from '../../styles/globalStyles';
import { MainLabel, HintLabel, Button, DismissKeyboardView } from '../ui';

import { resendVerificationCode } from '../../redux/actions/login'

const EnterSmsForm = ({ nextStep, phoneNumber }) => {
    const { error } = useSelector(state => state.register);
    const [code, setCode] = useState('');
    const [timer, setTimer] = useState(60);
    const dispatch = useDispatch();

    const onFulfillHandler = code => {
        nextStep({
            phone_number: phoneNumber,
            code: Number(code)
        });
    }

    const startTimer = () => {
        dispatch(resendVerificationCode(phoneNumber));
        setTimer(60);
    }

    useEffect(() => {
        let timeoutId = setTimeout(() => {
            setTimer(prevTimer => prevTimer - 1);
        }, 1000)

        if(timer === 0) {
            clearTimeout(timeoutId);
        }

        return () => {
            clearTimeout(timeoutId);
        }
    }, [timer]);
    
    return (
        <DismissKeyboardView>
            <View style={styles.container}>
                <View style={styles.code}>
                    <MainLabel style={{ marginTop: 40 }}>Введите код, отправленный на ваш телефон</MainLabel>
                    <HintLabel style={{ marginBottom: 15 }}>Код отправлен на номер{"\n"}+7 {phoneNumber}</HintLabel>

                    <SmoothPinCodeInput
                        value={code}
                        onTextChange={code => setCode(code)}
                        onFulfill={code => onFulfillHandler(code)}
                        password
                        mask="•"
                        cellStyle={{
                            borderWidth: 2,
                            borderRadius: 15,
                            borderColor: error ? colors.red : colors.gray,
                            height: 60,
                            width: 60,
                        }}
                        textStyle={{
                            fontSize: 24,
                            color: colors.white
                        }}
                        cellSpacing={18}
                        keyboardType="numeric"
                    />
                    {timer > 0 ? (
                        <Text style={styles.codeAgainLabel}>Отправить код повторно через {timer}</Text>
                    ) : null}
                </View>

                {timer === 0 ? (
                    <Button
                        type="success"
                        text="Отправить код"
                        onPress={startTimer}
                    />
                ) : null}
            </View>
        </DismissKeyboardView>
    )
}

export default EnterSmsForm

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 30,
        justifyContent: 'space-between'
    },
    code: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'column',
    },
    codeAgainLabel: {
        fontSize: 14,
        color: colors.white,
        textAlign: 'center',
        marginTop: 32
    }
})

import React from 'react'
import { StyleSheet, Text, TouchableOpacity} from 'react-native'
import { colors } from '../../styles/globalStyles'

const PoliticsAlert = () => {
    return (
        <Text style={styles.politicsText}>
            Нажимая кнопку ниже, вы подтверждаете, что прочитали и принимаете{" "}
            <TouchableOpacity>
                <Text style={styles.highlightedText}>Условия использования</Text>
            </TouchableOpacity> и {"\n"}
            <TouchableOpacity>
                <Text style={styles.highlightedText}>Политика безопасности</Text>
            </TouchableOpacity>
        </Text>
    )
}

export default PoliticsAlert

const styles = StyleSheet.create({
    politicsText: {
        color: colors.white,
        fontSize: 12,
        textAlign: 'center',
        marginBottom: 17
    },
    highlightedText: {
        color: colors.green,
        textDecorationLine: 'underline',
        fontSize: 12,
        margin: 0,
        padding: 0
    },
})

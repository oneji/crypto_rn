import React from 'react'
import { StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from '../../ui/Icon';

const HeaderButton = ({ icon, color, onPress }) => {
    return (
        <TouchableOpacity style={styles.btn} onPress={onPress}>
            <Icon
                type={icon}
                color={color}
            />
        </TouchableOpacity>
    )
}

export default HeaderButton

const styles = StyleSheet.create({
    btn: {
        width: 70,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

import React from 'react'
import { TouchableOpacity } from 'react-native'
import { colors } from '../../../styles/globalStyles'
import Icon from '../../ui/Icon'

const ShowPasswordButton = (props) => {
    return (
        <TouchableOpacity {...props}>
            <Icon type="eye" color={colors.white} />
        </TouchableOpacity>
    )
}

export default ShowPasswordButton

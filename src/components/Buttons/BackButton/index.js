import React from 'react'
import { StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from '../../ui/Icon';
import { colors } from '../../../styles/globalStyles'

const BackButton = (props) => {
    return (
        <TouchableOpacity
            {...props}
            style={styles.btn}
        >
            <Icon
                type="arrowLeft"
                color={colors.white}
            />
        </TouchableOpacity>
    )
}

export default BackButton

const styles = StyleSheet.create({
    btn: {
        width: 50,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

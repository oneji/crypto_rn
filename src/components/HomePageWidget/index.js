import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { colors } from '../../styles/globalStyles'
import IconButton from '../ui/IconButton'
import MainLabel from '../ui/Labels/MainLabel'
import Icon from '../ui/Icon'

const HomePageWidget = () => {
    return (
        <View>
            <MainLabel>Общий баланс</MainLabel>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    paddingVertical: 11
                }}
            >
                <Text style={styles.dollarSign}>$</Text>
                <Text style={styles.moneyAmount}>3,625.36</Text>
                <Text style={styles.currency}>USD</Text>
            </View>
            
            <Text
                style={{
                    color: colors.green,
                    textAlign: 'center',
                    fontSize: 14
                }}
            >
                <Icon type="arrowTop" color={colors.green} /> 
                +946,21 FNK (+2.91%)
            </Text>

            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 27
                }}
            >
                <IconButton icon="download">Пополнить</IconButton>
                <IconButton icon="redo">Перевод</IconButton>
                <IconButton icon="transfer">Обмен</IconButton>
                <IconButton icon="withdraw">Вывод</IconButton>
            </View>
        </View>
    )
}

export default HomePageWidget

const styles = StyleSheet.create({
    dollarSign: {
        fontSize: 20,
        color: colors.grayText,
        alignSelf: 'flex-start'
    },
    moneyAmount: {
        fontSize: 38,
        color: colors.white,
        marginLeft: 8,
        marginRight: 42,
        fontFamily: 'SFProText-Medium',
    },
    currency: {
        fontSize: 24,
        color: colors.grayText,
        alignSelf: 'flex-end'
    }
})

import React from 'react'
import { StyleSheet, View } from 'react-native'
import StableCoinGroupItem from './StableCoinGroupItem';

const StableCoinGroup = ({ items = [], active = 0, onPress }) => {
    return (
        <View style={styles.container}>
            {items.map((item, idx) => (
                <StableCoinGroupItem
                    item={item}
                    idx={idx}
                    active={active}
                    key={item.id}
                    onPress={onPress}
                />
            ))}
        </View>
    )
}

export default StableCoinGroup

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginTop: 28,
        width: '100%',
        justifyContent: 'space-around'
    }
})

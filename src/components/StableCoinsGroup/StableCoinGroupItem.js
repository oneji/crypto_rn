import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { colors } from '../../styles/globalStyles'

const StableCoinGroupItem = ({ item, idx, active, onPress }) => {
    return (
        <TouchableOpacity
            onPress={() => onPress({
                idx: idx,
                limit: item.limit
            })}
            style={{
                borderBottomColor: idx === active ? colors.green : 'transparent',
                borderBottomWidth: 1
            }}
        >
            <Text style={styles.text}>{item.name}</Text>
        </TouchableOpacity>
    )
}

export default StableCoinGroupItem

const styles = StyleSheet.create({
    text: {
        color: colors.white,
        fontSize: 16,
        textAlign: 'center',
        width: 70,
        marginBottom: 4
    }
})

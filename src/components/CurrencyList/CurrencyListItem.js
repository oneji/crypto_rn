import React from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import { colors } from '../../styles/globalStyles'
import Badge from '../ui/Badge'
import { LineChart } from 'react-native-svg-charts'
import * as shape from 'd3-shape'

const CurrencyListItem = ({ item }) => {
    return (
        <View style={styles.item}>
            <View style={styles.left}>
                <Image
                    style={{
                        borderColor: colors.grayText,
                        width: 32,
                        height: 32,
                        backgroundColor: colors.gray,
                        borderRadius: 100,
                        marginRight: 16
                    }} 
                />
                <View>
                    <Text
                        style={{
                            color: colors.white,
                            fontSize: 16,
                            fontFamily: 'SFProText-Medium',
                        }}
                    >
                        {item.title}
                    </Text>
                    <Text
                        style={{
                            color: colors.grayText,
                            fontSize: 14,
                            fontFamily: 'SFProText-Regular'
                        }}
                    >
                        {item.shortTitle}
                    </Text>
                </View>
            </View>

            <View style={styles.center}>
                <LineChart
                    style={{
                        height: '100%',
                        borderRadius: 100,
                    }}
                    data={[0, 500, 0, 1000, 1001]}
                    svg={{
                        strokeWidth: 2,
                        stroke: colors.green,
                    }}
                    contentInset={{
                        top: 12,
                        bottom: 12
                    }}
                    curve={shape.curveNatural}
                />
            </View>

            <View style={styles.right}>
                <Text
                    style={{
                        color: colors.white,
                        fontSize: 16,
                        fontFamily: 'SFProText-Medium',
                        marginBottom: 3    
                    }}
                >
                    {item.price}
                </Text>

                <Badge type={item.percentType === 'positive' ? 'success' : 'danger'}>
                    {item.percentType === 'positive' ? '+' : '-'}
                    {item.percent}% 
                </Badge>
            </View>
        </View>
    )
}

export default CurrencyListItem

const styles = StyleSheet.create({
    item: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        borderTopWidth: 0.5,
        borderColor: colors.grayBorderColor,
    },
    left: {
        flexGrow: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 5,
        minWidth: 130
    },
    center: {
        flexGrow: 1
    },
    right: {
        flexGrow: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 5,
        minWidth: 100
    }
});
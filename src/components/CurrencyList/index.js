import React from 'react'
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { colors } from '../../styles/globalStyles'
import CurrencyListItem from './CurrencyListItem'

const data = [
    { id: 1, title: 'Bitcoin', shortTitle: 'BTC', price: '$11,452.24', percent: '0.70', percentType: 'positive' },
    { id: 2, title: 'Tether', shortTitle: 'USDT', price: '$8,452.24', percent: '0.70', percentType: 'negative' },
    { id: 3, title: 'Ripple', shortTitle: 'XRP', price: '$5,452.24', percent: '0.70', percentType: 'positive' },
    { id: 4, title: 'Etherium', shortTitle: 'ETH', price: '$5,452.24', percent: '1.01', percentType: 'positive' },
    { id: 5, title: 'Etherium', shortTitle: 'ETH', price: '$3,000.00', percent: '1.20', percentType: 'negative' },
]

const CurrencyList = ({ items = data }) => {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.title}>Валюты</Text>
                <TouchableOpacity>
                    <Text style={styles.all}>Смотреть все</Text>
                </TouchableOpacity>
            </View>
            <FlatList 
                data={items}
                renderItem={({ item }) => (
                    <CurrencyListItem item={item} />
                )}
                keyExtractor={item => item.id.toString()}
                showsVerticalScrollIndicator={false}
            />
        </View>
    )
}

export default CurrencyList

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 55
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 15,
        alignItems: 'center'
    },
    title: {
        color: colors.white,
        fontSize: 16,
        fontFamily: 'SFProText-Medium'
    },
    all: {
        fontSize: 12,
        color: colors.blue
    }
})

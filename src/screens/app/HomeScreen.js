import React from 'react'
import { StyleSheet, View } from 'react-native'
import HomePageWidget from '../../components/HomePageWidget'
import CurrencyList from '../../components/CurrencyList'

const HomeScreen = () => {
    return (
        <View style={styles.container}>
            {/* Home page widget */}
            <HomePageWidget />

            {/* Currence list */}
            <CurrencyList />
        </View>
    )
}

export default HomeScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 43,
        paddingHorizontal: 24
    }
})

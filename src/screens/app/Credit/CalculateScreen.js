import React, { useState } from 'react'
import { StyleSheet, TextInput, View, Text, KeyboardAvoidingView } from 'react-native'
import { MainLabel, HintLabel, Button } from '../../../components/ui'
import { colors } from '../../../styles/globalStyles'
import StableCoinsGroup from '../../../components/StableCoinsGroup'

// Dummy data
const data = [
    { id: 1, name: 'TUSD', limit: 100.00 },
    { id: 2, name: 'PAX', limit: 110.00 },
    { id: 3, name: 'USDC', limit: 120.00 },
    { id: 4, name: 'USDT', limit: 130.00 },
];

const CalculateScreen = ({ navigation, route }) => {
    const [value, setValue] = useState('0')
    const [activeStableCoin, setActiveStableCoin] = useState(0);
    const [stableCoinLimit, setStableCoinLimit] = useState(data[0].limit);
    let { currency } = route.params;

    const handleOnActiveStableCoinChange = ({ idx, limit }) => {
        setActiveStableCoin(idx);
        setStableCoinLimit(limit); 
    }
    
    return (
        <KeyboardAvoidingView  
            behavior="padding"
            style={styles.container}
        >
            <View>
                <MainLabel>Выберите криптовалюту</MainLabel>
                <HintLabel>Выберите, какую криптовалюту использовать в качестве залога.</HintLabel>

                {/* Currency converter */}
                <View
                    style={{
                        alignItems: 'center',
                        marginTop: 24
                    }}
                >
                    <HintLabel>В наличии</HintLabel>
                    <View
                        style={{
                            borderColor: colors.grayBorderColor,
                            borderBottomWidth: .5,
                            width: 100,
                            flexDirection: 'row',
                            alignItems: 'flex-end',
                            justifyContent: 'center'
                        }}
                    >
                        <TextInput
                            keyboardType="number-pad"
                            value={value}
                            placeholder={'0'}
                            placeholderTextColor={colors.white}
                            onChangeText={value => setValue(value)}
                            style={{
                                fontSize: 24,
                                color: colors.white,
                                textAlign: 'center',
                                padding: 0,
                                marginRight: 5
                            }}
                        />
                        <Text
                            style={{
                                color: colors.grayText,
                                fontSize: 24
                            }}
                        >{currency}</Text>
                    </View>

                    <HintLabel
                        style={{
                            marginTop: 24
                        }}
                    >
                        Подключить кредит в
                    </HintLabel>
                    <Text
                        style={{
                            color: colors.white,
                            fontSize: 24
                        }}          
                    >
                        0.00
                    </Text>
                    
                    <StableCoinsGroup
                        items={data}
                        active={activeStableCoin}
                        onPress={handleOnActiveStableCoinChange}
                    />
                </View>
            </View>

            <Button
                type="success"
                text={value < stableCoinLimit ? `Ниже мин. лимита ${stableCoinLimit}` : 'Пополнить'}
                disabled={value < stableCoinLimit}
                style={{
                    marginTop: 32,
                    marginBottom: 16,

                }}
            />
        </KeyboardAvoidingView>
    )
}

export default CalculateScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 24,
        paddingHorizontal: 24,
        justifyContent: 'space-between'
    }
})

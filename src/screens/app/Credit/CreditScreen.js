import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

import MainLabel from '../../../components/ui/Labels/MainLabel'
import { Checkbox, RadionButton } from '../../../components/ui'
import Button from '../../../components/ui/Button'
import { colors } from '../../../styles/globalStyles'
import { FlatList } from 'react-native-gesture-handler'

const data = [
    { id: 1, title: 'BTC обеспечение', percent: '7%' },
    { id: 2, title: 'BTC обеспечение', percent: '7%' },
    { id: 3, title: 'BTC обеспечение', percent: '7%' },
    { id: 4, title: 'BTC обеспечение', percent: '7%' },
    { id: 5, title: 'BTC обеспечение', percent: '7%' }
];

const CreditScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <View style={{ flex: 1 }}>
                <MainLabel style={{ marginBottom: 24 }}>Пополните Ваш кошелёк и получите мгновенный кредит</MainLabel>
                <Checkbox circle size={24} label="Платите в удобное для вас время - без пени и сроков." />
                <Checkbox circle size={24} label="Внесите свою криптовалюту в качестве залога и получите кредит TUSD/PAX/USDC/USDT." /> 

                <View
                    style={{
                        borderWidth: 1,
                        borderColor: colors.grayBorderColor,
                        paddingVertical: 22,
                        paddingHorizontal: 16,
                        flex: 1,
                        borderRadius: 10
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            marginBottom: 14
                        }}
                    >
                        <RadionButton
                            size={16}
                            label="С холдированием 100 FNK или меньше"
                            style={{
                                marginBottom: 0,
                                flexShrink: 1,
                            }}
                            labelStyle={{
                                fontSize: 12
                            }}
                        />
                        <RadionButton
                            size={16}
                            label="С холдированием 1000 FNK или больше"
                            style={{
                                marginBottom: 0,
                                flexShrink: 1,
                            }}
                            labelStyle={{
                                fontSize: 12
                            }}
                        />
                    </View>
                    <FlatList
                        data={data}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item }) => (
                            <View
                                style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    borderTopWidth: 0.5,
                                    borderColor: colors.grayBorderColor,
                                    paddingVertical: 16,
                                    paddingHorizontal: 8,
                                }}
                            >
                                <Text
                                    style={{
                                        color: colors.white,
                                        fontSize: 16
                                    }}
                                >{item.title}</Text>
                                <Text
                                    style={{
                                        color: colors.white,
                                        fontSize: 16
                                    }}
                                >{item.percent}</Text>
                            </View>
                        )}
                        keyExtractor={item => item.id.toString()}
                    />
                </View>
            </View>

            <View>
                <Button
                    style={{
                        marginTop: 24
                    }}
                    type="success"
                    text="Пополнить сейчас"
                    onPress={() => navigation.navigate('CreditChooseCryptoScreen')}
                />
            </View>
        </View>
    )
}

export default CreditScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24,
        justifyContent: 'space-between',
        paddingVertical: 20
    },
})

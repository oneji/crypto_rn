import React from 'react'
import { StyleSheet, View, Text, FlatList, Image, TouchableOpacity } from 'react-native'
import { MainLabel, HintLabel } from '../../../components/ui'
import Icon from '../../../components/ui/Icon';
import { colors } from '../../../styles/globalStyles'

const data = [
    { id: 1, title: 'FNK', shortTitle: 'FNK' },
    { id: 2, title: 'Bitcoin', shortTitle: 'BTC' },
    { id: 3, title: 'Ethereum', shortTitle: 'ETH' },
    { id: 4, title: 'Ripple', shortTitle: 'XRP' },
    { id: 5, title: 'Cardano', shortTitle: 'ADA' },
    { id: 6, title: 'Litecoin', shortTitle: 'LTC' },
    { id: 7, title: 'Litecoin', shortTitle: 'LTC' }
];

const ChooseCryptoScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <MainLabel>Выберите криптовалюту</MainLabel>
            <HintLabel>Выберите, какую криптовалюту использовать в качестве залога.</HintLabel>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 10,
                    borderColor: colors.grayBorderColor,
                    flex: 1,
                    paddingTop: 24,
                    paddingHorizontal: 16
                }}
            >
                <Text
                    style={{
                        color: colors.grayText,
                        textTransform: 'uppercase',
                        marginBottom: 14
                    }}
                >
                    Криптовалюта
                </Text>

                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={data}
                    renderItem={({ item }) => (
                        <View
                            style={{
                                flexGrow: 1,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                paddingVertical: 12,
                                borderTopWidth: 0.5,
                                borderColor: colors.grayBorderColor,
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                }}
                            >
                                <Image
                                    style={{
                                        borderColor: colors.grayText,
                                        width: 32,
                                        height: 32,
                                        backgroundColor: colors.gray,
                                        borderRadius: 100,
                                        marginRight: 8
                                    }} 
                                />
                                <Text
                                    style={{
                                        color: colors.white,
                                        fontSize: 16,
                                        fontFamily: 'SFProText-Medium',
                                    }}
                                >
                                    {item.title}
                                </Text>
                            </View>

                            <TouchableOpacity
                                onPress={() => navigation.navigate('CalculateScreen', {
                                    currency: item.shortTitle
                                })}
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between'
                                }}
                            >
                                <Text
                                    style={{
                                        color: colors.white,
                                        fontSize: 16,
                                        fontFamily: 'SFProText-Medium',
                                        textTransform: 'uppercase',
                                        marginRight: 8
                                    }}
                                >
                                    {item.shortTitle}
                                </Text>
                                <Icon type="arrowRight" />
                            </TouchableOpacity>
                        </View>
                    )}
                    keyExtractor={item => item.id.toString()}
                />
            </View>
        </View>
    )
}

export default ChooseCryptoScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 24,
        paddingHorizontal: 24
    }
})

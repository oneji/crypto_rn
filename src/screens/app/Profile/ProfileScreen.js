import React from 'react'
import { StatusBar, View, StyleSheet, Text } from 'react-native'
import { colors } from '../../../styles/globalStyles'

const CardScreen = ({ navigation }) => {
    return (
        <>
        <StatusBar backgroundColor={colors.dark} />
        <View style={styles.container}>
            <Text style={styles.title}>Профиль</Text>
        </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        color: colors.white,
        textAlign: 'center',
        fontSize: 24,
    }
})

export default CardScreen
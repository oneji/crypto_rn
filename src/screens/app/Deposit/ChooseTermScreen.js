import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { HintLabel, MainLabel, RadionButton } from '../../../components/ui'
import { colors } from '../../../styles/globalStyles'

const ChooseTermScreen = () => {
    return (
        <View style={styles.container}>
            <MainLabel>Выберите срок</MainLabel>
            <HintLabel>Когда срок истечет, ваша основная сумма и прибыль будут в выбранной вами валюте.</HintLabel>

            <RadionButton
                size={20}
                label="Гибкий"
                description="Активы могут быть сняты в любое время и будут приносить 1.50% в год в течение всего срока."
                labelStyle={{
                    color: colors.white,
                    fontSize: 16
                }}
            />
            <RadionButton
                size={20}
                label="1 месяц"
                description="Активы будут храниться в течение 1 месяца и будут приносить 3% годовых в течение всего этого срока."
                labelStyle={{
                    color: colors.white,
                    fontSize: 16
                }}
            />
            <RadionButton
                size={20}
                label="3 месяца"
                description="Активы будут храниться в течение 3 месяцев и будут приносить 4,50% годовых в течение всего срока."
                labelStyle={{
                    color: colors.white,
                    fontSize: 16
                }}
            />
        </View>
    )
}

export default ChooseTermScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 24,
        paddingHorizontal: 24
    }
})

import React from 'react'
import { FlatList, StyleSheet, Text, View } from 'react-native'
import { Button, Checkbox, MainLabel, RadionButton } from '../../../components/ui';
import { colors } from '../../../styles/globalStyles'

const data = [
    { id: 1, title: 'BTC обеспечение', percent: '7%' },
    { id: 2, title: 'BTC обеспечение', percent: '7%' },
    { id: 3, title: 'BTC обеспечение', percent: '7%' },
    { id: 4, title: 'BTC обеспечение', percent: '7%' },
    { id: 5, title: 'BTC обеспечение', percent: '7%' },
    { id: 6, title: 'BTC обеспечение', percent: '7%' },
    { id: 7, title: 'BTC обеспечение', percent: '7%' },
    { id: 8, title: 'BTC обеспечение', percent: '7%' },
];

const DepositScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <View style={{ flex: 1 }}>
                <MainLabel style={{ marginBottom: 24 }}>Криптоактивы</MainLabel>
                <Checkbox size={24} label="Заработок будет зачисляться на ваш кошелек каждые 7 дней." />
                <Checkbox size={24} label="Заработок выплачивается в той же валюте, что и ваш вклад." /> 

                <View
                    style={{
                        borderWidth: 1,
                        borderColor: colors.grayBorderColor,
                        paddingVertical: 22,
                        paddingHorizontal: 16,
                        flex: 1,
                        borderRadius: 10
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            marginBottom: 14
                        }}
                    >
                        <RadionButton
                            size={16}
                            label="С холдированием 100 FNK или меньше"
                            style={{
                                marginBottom: 0,
                                flexShrink: 1,
                            }}
                            labelStyle={{
                                fontSize: 12
                            }}
                        />
                        <RadionButton
                            size={16}
                            label="С холдированием 1000 FNK или больше"
                            style={{
                                marginBottom: 0,
                                flexShrink: 1,
                            }}
                            labelStyle={{
                                fontSize: 12
                            }}
                        />
                    </View>
                    
                    <FlatList
                        data={data}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item }) => (
                            <View
                                style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    borderTopWidth: 0.5,
                                    borderColor: colors.grayBorderColor,
                                    paddingVertical: 16,
                                    paddingHorizontal: 8,
                                }}
                            >
                                <Text
                                    style={{
                                        color: colors.white,
                                        fontSize: 16
                                    }}
                                >{item.title}</Text>
                                <Text
                                    style={{
                                        color: colors.white,
                                        fontSize: 16
                                    }}
                                >{item.percent}</Text>
                            </View>
                        )}
                        keyExtractor={item => item.id.toString()}
                    />
                </View>
            </View>

            <View>
                <Button
                    style={{
                        marginTop: 24
                    }}
                    type="success"
                    text="Начните зарабатывать сейчас"
                    onPress={() => navigation.navigate('DepositChooseCryptoScreen')}
                />
            </View>
        </View>
    )
}

export default DepositScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24,
        justifyContent: 'space-between',
        paddingVertical: 20
    }
})

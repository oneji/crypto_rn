import React from 'react'
import { Image, StatusBar, View, StyleSheet, Text } from 'react-native'
import { Button } from '../../../components/ui'
import { colors } from '../../../styles/globalStyles'

const CardScreen = ({ navigation }) => {
    return (
        <>
        <StatusBar backgroundColor={colors.dark} />
        <View style={styles.container}>
            <View>
                <Image
                    source={require('../../../assets/images/card.png')}
                    style={styles.image}
                    resizeMode="contain"
                />

                <Text style={styles.title}>Получите свою первую карту</Text>
            </View>

            <View>
                <Button
                    type="success"
                    text="Оформить карту"
                    style={{
                        marginBottom: 23
                    }}
                />
            </View>
        </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24,
        justifyContent: 'space-between',
    },
    title: {
        color: colors.white,
        textAlign: 'center',
        fontSize: 24,
        textTransform: 'uppercase',
        marginTop: 30,
        fontFamily: 'SFProText-Bold'
    },
    image: {
        width: '100%',
        height: 260,
        marginTop: 50
    }
})

export default CardScreen
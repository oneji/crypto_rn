import React, { useState } from 'react'
import { View, StyleSheet } from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup'
import { Button, Input, HintLabel, TouchableLabel, DismissKeyboardView } from '../components/ui';

// Redux actions
import { nextStep } from '../redux/actions/wizard'
import { setEmail } from '../redux/actions/register'
import { useDispatch } from 'react-redux';

// Validation schema
const EmailSchema = Yup.object().shape({
    email: Yup.string()
        .trim()
        .email('Пожалуйста введите действительный электронный адрес')
        .required('Заполните пустые поля.'),
  });

const EnterEmailScreen = () => {
    const dispatch = useDispatch();

    const emailSubmitHandler = (values) => {
        dispatch(setEmail(values.email));
        dispatch(nextStep());
    }

    return (
        <Formik
            initialValues={{ email: '' }}
            onSubmit={emailSubmitHandler}
            validationSchema={EmailSchema}
        >
            {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
                <DismissKeyboardView>
                    <View style={styles.container}>
                        <View>
                            <Input
                                keyboardType="email-address"
                                placeholder="Введите email"
                                label="Email"
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                                value={values.email}
                                error={errors.email}
                            />
                            <HintLabel>
                                Это необходимо для завершения регистрации. 
                                Подтвердите свой адрес электронной почты позже, 
                                чтобы получать сообщения о вашей учетной записи или подписках
                            </HintLabel>
                        </View>

                        <View>
                            <TouchableLabel
                                onPress={() => dispatch(nextStep())}
                                style={{
                                    marginBottom: 29
                                }}
                            >
                                Пропустить
                            </TouchableLabel>

                            <Button
                                type="success"
                                text="Далее"
                                onPress={handleSubmit}
                            />
                        </View>
                    </View>
                </DismissKeyboardView>
            )}
        </Formik>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        paddingTop: 21
    }
});

export default EnterEmailScreen;
import React from 'react'
import { Image, StatusBar, Text, View, StyleSheet } from 'react-native'
import Button from '../components/ui/Button'
import MainLabel from '../components/ui/Labels/MainLabel'
import { colors } from '../styles/globalStyles'

const HelloScreen = ({ navigation }) => {
    return (
        <>
        <StatusBar backgroundColor={colors.dark} />
        <View style={styles.container}>
            <View>
                <Image
                    source={require('../assets/images/login.png')}
                    style={styles.image}
                    resizeMode="contain"
                />

                <Text style={styles.title}>Fnk wallet</Text>
                <MainLabel>Готовые решения для{"\n"}максимального профита</MainLabel>
            </View>

            <View>
                <Button
                    type="success"
                    text="Регистрация"
                    style={{
                        marginBottom: 23
                    }}
                    onPress={() => navigation.navigate('RegisterScreen')}
                />
                <Button
                    text="Войти в кабинет"
                    onPress={() => navigation.navigate('LoginScreen')}
                />
            </View>
        </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24,
        justifyContent: 'space-between',
        paddingVertical: 50
    },
    title: {
        color: colors.white,
        textAlign: 'center',
        fontSize: 28,
        textTransform: 'uppercase',
        marginBottom: 23,
        marginTop: 30,
        fontFamily: 'SFProText-Bold'
    },
    image: {
        width: '100%',
        height: 250
    }
})

export default HelloScreen
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { StyleSheet, View } from 'react-native'
import { colors } from '../styles/globalStyles'
import { Formik } from 'formik'
import * as Yup from 'yup'

import SmoothPinCodeInput from 'react-native-smooth-pincode-input'
import { Button, MainLabel, HintLabel, DismissKeyboardView } from '../components/ui'

// Validation schema
const PinSchema = Yup.object().shape({
    code: Yup.string()
        .required('Заполните пустые поля.'),
    codeConfirm: Yup.string()
        .oneOf([Yup.ref('code')], 'PIN не совпадает.')
        .required('Заполните пустые поля.'),
});

const PinScreen = ({ phoneNumber, nextStep }) => {
    const dispatch = useDispatch();
    const { email, loading } = useSelector(state => state.register);
    const [code, setCode] = useState('');
    const [codeConfirm, setCodeConfirm] = useState('');

    const onPinSubmitHandler = (values) => {
        // If vetification code is corrent navigate to StartScreen
        console.log(values);

        if(code === codeConfirm) {
            if(email) {
                nextStep({
                    email: email,
                    phone_number: phoneNumber,
                    pin_code: values.code,
                    pin_code_confirmation: values.codeConfirm
                })
            } else {
                nextStep({
                    phone_number: phoneNumber,
                    pin_code: values.code,
                    pin_code_confirmation: values.codeConfirm
                })
            }
        }
    }

    return (
        <Formik
            initialValues={{ code: '', codeConfirm: '' }}
            validationSchema={PinSchema}
            onSubmit={onPinSubmitHandler}
        >
            {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
                <DismissKeyboardView>
                    <View style={styles.container}>
                        <View style={styles.code}>
                            <MainLabel
                                style={{
                                    marginTop: 28
                                }}
                            >
                                Чтобы ваши средства были в безопасности, установите 4-значный PIN
                            </MainLabel>
        
                            <HintLabel>Установите новый PIN</HintLabel>
        
                            <SmoothPinCodeInput
                                value={values.code}
                                onTextChange={handleChange('code')}
                                password
                                mask="•"
                                cellStyle={{
                                    borderWidth: 2,
                                    borderRadius: 15,
                                    borderColor: errors.code ? colors.red : colors.gray,
                                    height: 60,
                                    width: 60,
                                }}
                                textStyle={{
                                    fontSize: 24,
                                    color: colors.white
                                }}
                                keyboardType="phone-pad"
                                cellSpacing={18}
                            />

                            <HintLabel style={{ marginTop: 56 }}>Подтвердите PIN</HintLabel>
                            <SmoothPinCodeInput
                                value={values.codeConfirm}
                                onTextChange={handleChange('codeConfirm')}
                                password
                                mask="•"
                                cellStyle={{
                                    borderWidth: 2,
                                    borderRadius: 15,
                                    borderColor: errors.codeConfirm ? colors.red : colors.gray,
                                    height: 60,
                                    width: 60,
                                }}
                                textStyle={{
                                    fontSize: 24,
                                    color: colors.white
                                }}
                                keyboardType="phone-pad"
                                cellSpacing={18}
                            />
                        </View>
        
                        <Button
                            type="success"
                            text="Зарегистрировать аккаунт"
                            loading={loading}
                            onPress={handleSubmit}
                        />
                    </View>
                </DismissKeyboardView>
            )}
        </Formik>
    )
}

export default PinScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
    },
    title: {
        color: colors.white,
        fontSize: 20,
        fontFamily: 'SFProText-Medium',
        textAlign: 'center',
        marginTop: 28,
        marginBottom: 18
    },
    code: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'column',
    },
})

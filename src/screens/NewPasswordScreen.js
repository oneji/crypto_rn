import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'
import { Formik } from 'formik';
import { Input, Button, MainLabel, DismissKeyboardView } from '../components/ui'
import * as Yup from 'yup'

import { AuthService } from '../services'

// Validation schema
const NewPasswordSchema = Yup.object().shape({
    password: Yup.string()
        .trim()
        .matches(/^(?=.*\d)/, { message: 'Должен содержать хотя бы одно число' })
        .min(6, 'Должно быть не менее 6 символов')
        .required('Заполните пустые поля.')
});

const NewPasswordScreen = ({ route, navigation }) => {
    const [loading, setLoading] = useState(false);
    const { phoneNumber } = route.params;

    const onNewPasswordHandler = async ({ password }) => {
        try {
            setLoading(true);
            let { data } = await AuthService.updatePassword({
                phoneNumber: phoneNumber,
                password: password
            });

            if(data.status) {
                navigation.navigate('RestoreSuccessScreen');
            }
            setLoading(false);
        } catch (error) {
            setLoading(false);
        }
    }

    return (
        <Formik
            initialValues={{ password: '' }}
            onSubmit={onNewPasswordHandler}
            validationSchema={NewPasswordSchema}
        >
            {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
                <DismissKeyboardView>
                    <View style={styles.container}>
                        <View>
                            <MainLabel>
                                Создание нового пароля
                            </MainLabel>
                            <Input
                                type="password"
                                placeholder="Введите новый пароль"
                                label="Пароль"
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                                value={values.password}
                                error={errors.password}
                            />
                        </View>

                        <View>
                            <Button
                                type="success"
                                text="Сохранить"
                                loading={loading}
                                disabled={loading}
                                onPress={handleSubmit} />
                        </View>
                    </View>
                </DismissKeyboardView>
            )}
        </Formik>
    )
}

export default NewPasswordScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24,
        justifyContent: 'space-between',
        paddingVertical: 21
    },
})

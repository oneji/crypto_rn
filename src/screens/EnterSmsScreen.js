import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Alert } from 'react-native'
import EnterSmsForm from '../components/EnterSmsForm'

import { AuthService } from '../services'

const EnterSmsScreen = ({ route, navigation }) => {
    const { phoneNumber } = route.params;

    const onNextHandler = async (values) => {        
        try {
            let { data } = await AuthService.verifyRestore({
                phoneNumber: values.phone_number,
                code: values.code
            });

            if(data.status) {
                navigation.navigate('NewPasswordScreen', {
                    phoneNumber: phoneNumber
                });
            }            
        } catch (error) {
            console.log('error', error.response);
            Alert.alert('Ошибка', error.response.data.error);
        }
    }

    return (
        <View style={styles.container}>
            <EnterSmsForm
                phoneNumber={phoneNumber}
                nextStep={onNextHandler}
            />
        </View>
    )
}

export default EnterSmsScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 30,
        paddingHorizontal: 24,
        justifyContent: 'space-between',
    }
})

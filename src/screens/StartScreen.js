import React, { useState } from 'react'
import { Image, View, StyleSheet } from 'react-native'
import { useDispatch } from 'react-redux'
import { Button, MainLabel } from '../components/ui'

import { AuthService } from '../services'
import { loginSuccess } from '../redux/actions/login'

const StartScreen = ({ route }) => {
    const [loading, setLoading] = useState(false);
    const { user, token } = route.params;
    const dispatch = useDispatch();

    const startHandler = async () => {
        try {
            setLoading(true);
            await AuthService.storeToken(token);
            await AuthService.storeUserObject(user);
            dispatch(loginSuccess({ user, token }));
        } catch (error) {
            console.log(error)
        } finally {
            setLoading(false);
        }
    }

    return (
        <>
        <View style={styles.container}>
            <View>
                <Image
                    source={require('../assets/images/register_final.png')}
                    style={styles.image}
                    resizeMode="contain"
                />

                <MainLabel
                    style={{
                        marginTop: 81
                    }}
                >
                    Ваша учетная запись была успешно создана
                </MainLabel>
            </View>

            <View>
                <Button
                    onPress={startHandler}
                    type="success"
                    text="Старт"
                    loading={loading}
                    disabled={loading}
                    style={{
                        marginBottom: 23
                    }}
                />
            </View>
        </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24,
        justifyContent: 'space-between',
        paddingVertical: 20
    },
    image: {
        width: '100%',
        height: 250
    }
})

export default StartScreen
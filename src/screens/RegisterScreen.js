import React, { useEffect, useRef } from 'react'
import { View, StyleSheet, Text } from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup'
import { colors } from '../styles/globalStyles'
import { Button, DismissKeyboardView, TouchableLabel, Input } from '../components/ui';
import PoliticsAlert from '../components/PoliticsAlert';

// React redux hooks
import { useSelector, useDispatch } from 'react-redux'
// Redux actions
import { register } from '../redux/actions/register'
import { resetSteps } from '../redux/actions/wizard'

const RegisterSchema = Yup.object().shape({
    phone_number: Yup.string()
        .min(10, 'Должно быть не менее 11 символов')
        .required('Заполните пустые поля.'),
    password: Yup.string()
        .trim()
        .matches(/^(?=.*\d)/, { message: 'Должен содержать хотя бы одно число' })
        .min(6, 'Должно быть не менее 6 символов')
        .required('Заполните пустые поля.')
  });

const RegisterScreen = ({ navigation }) => {
    const { loading, error } = useSelector(state => state.register);
    const dispatch = useDispatch();
    const formRef = useRef();

    useEffect(() => {
        dispatch(resetSteps());
        
        if(typeof error === 'object' && error) {
            formRef.current.setErrors(error);
        }

        return () => {
            formRef.current.setErrors({});
        }
    }, [error]);

    const registerSubmitHandler = (values) => {
        // If values are valid move to the EnterSmsScreen...
        console.log(values)

        dispatch(register({
            phone_number: values.phone_number,
            password: values.password
        }));
    }

    return (
        <Formik
            initialValues={{ phone_number: '', password: '' }}
            onSubmit={registerSubmitHandler}
            validationSchema={RegisterSchema}
            innerRef={formRef}
        >
            {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
                <DismissKeyboardView>
                    <View style={styles.container}>
                        <View>
                            <Input
                                type="phone"
                                keyboardType="phone-pad"
                                placeholder="Номер телефона"
                                label="Введите номер телефона"
                                style={{
                                    marginBottom: 10
                                }}
                                onChangeText={handleChange('phone_number')}
                                onBlur={handleBlur('phone_number')}
                                value={values.phone_number}
                                error={errors.phone_number}
                            />
                            
                            <Input
                                type="password"
                                placeholder="Введите пароль"
                                label="Создайте пароль"
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                                value={values.password}
                                error={errors.password}
                            />

                            {typeof error === 'string' && error ? (
                                <Text
                                    style={{
                                        fontSize: 12,
                                        color: colors.red,
                                        textAlign: 'center'
                                    }}
                                >{error}</Text>
                            ) : null}
                        </View>

                        <View>
                            <PoliticsAlert />

                            <Button
                                type="success"
                                text="Далее"
                                loading={loading}
                                disabled={loading}
                                onPress={handleSubmit} />

                            <TouchableLabel
                                onPress={() => {}}
                                style={{
                                    marginTop: 16
                                }}
                            >
                                У вас есть промокод?
                            </TouchableLabel>
                        </View>
                    </View>
                </DismissKeyboardView>
            )}
        </Formik>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24,
        justifyContent: 'space-between',
        paddingVertical: 21,
    },
});

export default RegisterScreen;
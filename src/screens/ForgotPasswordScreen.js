import React, { useRef, useState } from 'react'
import { View, StyleSheet } from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup'
import { Button, Input, MainLabel, HintLabel, DismissKeyboardView } from '../components/ui';

import { AuthService } from '../services'

// Validation schema
const ForgotPasswordSchema = Yup.object().shape({
    phoneNumber: Yup.string().required('Заполните пустые поля.')
});

const ForgotPasswordScreen = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const formRef = useRef();

    const onSubmitHandler = async (values) => {
        try {
            setLoading(true);
            let { data } = await AuthService.restorePassword(values.phoneNumber);

            // SHOW VERIFICATION CODE IN ALERT
            // Alert.alert('Код подтверждения', data.verification_code.toString());
            // ********************************
            
            navigation.navigate('EnterSmsScreen', {
                phoneNumber: values.phoneNumber
            });
        } catch (error) {
            console.log('error', error.response)

            formRef.current.setFieldError('phoneNumber', error.response.data.error);
        } finally {
            setLoading(false)
        }
    }

    return (
        <Formik
            innerRef={formRef}
            initialValues={{ phoneNumber: '' }}
            onSubmit={onSubmitHandler}
            validationSchema={ForgotPasswordSchema}
        >
            {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
                <DismissKeyboardView>
                    <View style={styles.container}>
                        <View>
                            <MainLabel>Забыли пароль?</MainLabel>
                            <HintLabel>Введите ниже свой регистрационный номер телефона, чтобы получить инструкцию по сбросу пароля.</HintLabel>
                            
                            <Input
                                type="phone"
                                keyboardType="phone-pad"
                                placeholder="Номер телефона"
                                label="Введите номер телефона"
                                style={{
                                    marginBottom: 10
                                }}
                                onChangeText={handleChange('phoneNumber')}
                                onBlur={handleBlur('phoneNumber')}
                                value={values.phoneNumber}
                                error={errors.phoneNumber}
                            />
                        </View>

                        <View>
                            <Button
                                type="success"
                                text="Отправить"
                                loading={loading}
                                onPress={handleSubmit} />
                        </View>
                    </View>
                </DismissKeyboardView>
            )}
        </Formik>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24,
        justifyContent: 'space-between',
        paddingVertical: 21
    },
});

export default ForgotPasswordScreen;
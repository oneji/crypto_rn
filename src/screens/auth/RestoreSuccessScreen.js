import React from 'react'
import { Image, View, StyleSheet } from 'react-native'
import { Button, MainLabel } from '../../components/ui'
import { colors } from '../../styles/globalStyles'

const RestoreSuccessScreen = ({ navigation }) => {
    return (
        <>
        <View style={styles.container}>
            <View>
                <Image
                    source={require('../../assets/images/register_final.png')}
                    style={styles.image}
                    resizeMode="contain"
                />

                <MainLabel
                    style={{
                        marginTop: 81
                    }}
                >
                    Ваш пароль был успешно обновлен
                </MainLabel>
            </View>

            <View>
                <Button
                    onPress={() => navigation.navigate('LoginScreen')}
                    type="success"
                    text="Войти"
                    style={{
                        marginBottom: 23
                    }}
                />
            </View>
        </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24,
        justifyContent: 'space-between',
        paddingVertical: 20
    },
    image: {
        width: '100%',
        height: 250
    }
})

export default RestoreSuccessScreen
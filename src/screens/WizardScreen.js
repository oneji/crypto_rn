import React from 'react';
import { StyleSheet, View } from 'react-native';
import Wizard from '../components/ui/Wizard/Wizard';
import EnterEmailScreen from './EnterEmailScreen';
import PinScreen from './PinScreen';
import EnterSmsForm from '../components/EnterSmsForm';

import { useDispatch } from 'react-redux';
import { completeRegister } from '../redux/actions/register';

const WizardScreen = ({ route, navigation }) => {
    const dispatch = useDispatch();
    const { phoneNumber } = route.params;

    const onFinishHandler = (data) => {
        dispatch(completeRegister(data));
    }

    const changeScreenHeaderTitle = (headerTitle) => {
        navigation.setOptions({
            title: headerTitle
        });
    }

    return (
        <View style={styles.container}>
            <Wizard
                onFinish={onFinishHandler}
                onTitleChange={changeScreenHeaderTitle}
            >
                <Wizard.Step>
                    <EnterSmsForm phoneNumber={phoneNumber} />
                </Wizard.Step>

                <Wizard.Step>
                    <EnterEmailScreen phoneNumber={phoneNumber} />
                </Wizard.Step>

                <Wizard.Step>
                    <PinScreen phoneNumber={phoneNumber} />
                </Wizard.Step>
            </Wizard>
        </View>
    )
}

export default WizardScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24
    }
})

import React, { useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { View, StyleSheet, Text } from 'react-native';
import { colors } from '../styles/globalStyles';
import { Formik } from 'formik';
import * as Yup from 'yup'
import { Button, Input, TouchableLabel, DismissKeyboardView } from '../components/ui';

// Redux actions
import { login } from '../redux/actions/login'

// Validation schema
const LoginSchema = Yup.object().shape({
    phone_number: Yup.string()
        .min(10, 'Должно быть не менее 11 символов')
        .required('Заполните пустые поля.'),
    password: Yup.string()
        .trim()
        .matches(/^(?=.*\d)/, { message: 'Должен содержать хотя бы одно число' })
        .min(6, 'Должно быть не менее 6 символов')
        .required('Заполните пустые поля.')
  });

const LoginScreen = ({ navigation }) => {
    const { error } = useSelector(state => state.user);
    const { loading } = useSelector(state => state.user);
    const dispatch = useDispatch();
    const formRef = useRef();

    const loginSubmitHandler = (values) => {
        // If values are valid move to the EnterSmsScreen...
        console.log(values)

        dispatch(login({
            phone_number: values.phone_number,
            password: values.password
        }));
    }

    return (
        <Formik
            innerRef={formRef}
            initialValues={{ phone_number: '', password: '' }}
            onSubmit={loginSubmitHandler}
            validationSchema={LoginSchema}
        >
            {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
                <DismissKeyboardView>
                    <View style={styles.container}>
                        <View>
                            <Input
                                type="phone"
                                keyboardType="phone-pad"
                                placeholder="Номер телефона"
                                label="Введите номер телефона"
                                style={{
                                    marginBottom: 10
                                }}
                                onChangeText={handleChange('phone_number')}
                                onBlur={handleBlur('phone_number')}
                                value={values.phone_number}
                                error={errors.phone_number}
                            />
                            
                            <Input
                                type="password"
                                placeholder="Введите пароль"
                                label="Пароль"
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                                value={values.password}
                                error={errors.password}
                            />

                            {typeof error === 'string' && error ? (
                                <Text
                                    style={{
                                        fontSize: 12,
                                        color: colors.red,
                                        textAlign: 'center'
                                    }}
                                >{error}</Text>
                            ) : null}
                        </View>

                        <View>
                            <TouchableLabel
                                onPress={() => navigation.navigate('ForgotPasswordScreen')}
                                style={{
                                    marginBottom: 29
                                }}
                            >
                                Забыли пароль?
                            </TouchableLabel>

                            <Button
                                type="success"
                                text="Вход"
                                loading={loading}
                                disabled={loading}
                                onPress={handleSubmit} />
                        </View>
                    </View>
                </DismissKeyboardView>
            )}
        </Formik>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24,
        justifyContent: 'space-between',
        paddingVertical: 21
    },
});

export default LoginScreen;
export const colors = {
    darkBg: '#1B1B1B',
    dark: '#202020',
    green: '#2CDCA1',
    white: '#FFFFFF',
    gray: '#707070',
    red: '#E53B3B',
    grayText: '#B0B0B0',
    blue: '#30C1FF',
    grayBorderColor: '#535A5C',
    grayBadge: '#2D2D2D',
    disabledGreenBtn: '#217C5C'
}
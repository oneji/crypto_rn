import * as types from '../constants/actionTypes'

export const register = credentials => ({
    type: types.REGISTER,
    payload: credentials
});

export const verify = ({ phone_number, code }) => ({
    type: types.VERIFY,
    payload: {
        phone_number,
        verify_code: code
    }
});

export const setEmail = email => ({
    type: types.SET_EMAIL,
    payload: email
});

export const registerSuccess = phone => ({
    type: types.REGISTER_SUCCESS,
    payload: phone
});

export const registerFailed = error => ({
    type: types.REGISTER_FAILED,
    payload: error
});

export const registerLoading = loading => ({
    type: types.REGISTER_LOADING,
    payload: loading
});

export const completeRegister = payload => ({
    type: types.COMPLETE_REGISTER,
    payload
})


import * as types from '../constants/actionTypes'
import { replace } from '../../navigation/RootNavigation'
import { AuthService } from '../../services';

export const login = credentials => ({
    type: types.LOGIN,
    payload: credentials
})

export const loginLoading = loading => ({
    type: types.LOGIN_LOADING,
    payload: loading
});

export const loginSuccess = credentials => ({
    type: types.LOGIN_SUCCESS,
    payload: credentials
});

export const loginFailed = error => ({
    type: types.LOGIN_FAILED,
    payload: error
});

export const logout = () => {
    AuthService.logout();
    
    return {
        type: types.LOGOUT
    }
}

export const resendVerificationCode = phoneNumber => ({
    type: types.RESEND_VERIFICATION_CODE,
    payload: phoneNumber
})
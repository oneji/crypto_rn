import * as types from '../constants/actionTypes'

export const nextStep = () => ({
    type: types.NEXT_STEP
});

export const resetSteps = () => ({
    type: types.RESET_STEPS
})


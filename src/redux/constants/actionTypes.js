// Register
export const REGISTER           = 'REGISTER'
export const REGISTER_LOADING   = 'REGISTER_LOADING'
export const REGISTER_SUCCESS   = 'REGISTER_SUCCESS'
export const REGISTER_FAILED    = 'REGISTER_FAILED'
export const SET_EMAIL          = 'SET_EMAIL'
export const COMPLETE_REGISTER  = 'COMPLETE_REGISTER'

// Verify
export const VERIFY             = 'VERIFY'
export const AUTH_LOADING       = 'AUTH_LOADING'

// Wizard
export const NEXT_STEP          = 'NEXT_STEP'
export const RESET_STEPS        = 'RESET_STEPS'

// Login
export const LOGIN              = 'LOGIN'
export const LOGIN_LOADING      = 'LOGIN_LOADING'
export const LOGIN_SUCCESS      = 'LOGIN_SUCCESS'
export const LOGIN_FAILED       = 'LOGIN_FAILED'
export const LOGOUT             = 'LOGOUT'
export const RESEND_VERIFICATION_CODE = 'RESEND_VERIFICATION_CODE'
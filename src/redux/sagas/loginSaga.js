import { put, takeEvery, call, all } from 'redux-saga/effects'
import { replace } from '../../navigation/RootNavigation'

import { AuthService } from '../../services'
import * as types from '../constants/actionTypes'
import * as actions from '../actions/login'

function* watchLogin() {
    yield takeEvery(types.LOGIN, login);
}

function* watchResendVerificationCode() {
    yield takeEvery(types.RESEND_VERIFICATION_CODE, resendVerificationCode);
}

function* login(action) {
    try {
        yield put(actions.loginLoading(true));

        const { data } = yield call(AuthService.login, action.payload);

        // Set token in AsyncStorage
        yield call(AuthService.storeToken, data.access_token);

        // Set user object in AsyncStorage
        yield call(AuthService.storeUserObject, data.user);

        yield put(actions.loginSuccess({
            user: data.user,
            token: data.access_token
        }));

        replace('AppStack');
    } catch (error) {
        let errorMessage = null;
        // Parse if errors is an object
        if(error.response.data.errors) {
            errorMessage = error.response.data.errors;
        } else {
            // Parse if error is a string
            errorMessage = error.response.data.error;
        }
        
        yield put(actions.loginFailed(errorMessage));
    } finally {
        yield put(actions.loginLoading(false));
    }
}

function* resendVerificationCode(action) {
    try {
        yield call(AuthService.resendVerificationCode, action.payload);
    } catch (error) {
        console.log(error);
    }
}

export function* loginSaga() {
    yield all([
        watchLogin(),
        watchResendVerificationCode()
    ]);
}
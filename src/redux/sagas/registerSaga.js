import { put, takeEvery, call, all } from 'redux-saga/effects'
import { navigate } from '../../navigation/RootNavigation'

import { Alert } from 'react-native'

import { AuthService } from '../../services'
import * as types from '../constants/actionTypes'
import * as actions from '../actions/register'

function* watchRegister() {
    yield takeEvery(types.REGISTER, register);
}

function* watchVerify() {
    yield takeEvery(types.VERIFY, verify);
}

function* watchCompleteRegister() {
    yield takeEvery(types.COMPLETE_REGISTER, completeRegister);
}

function* completeRegister(action) {
    try {
        yield put(actions.registerLoading(true));

        let { data } = yield call(AuthService.completeRegister, action.payload);

        navigate('StartScreen', {
            user: data.user,
            token: data.token
        });

        yield put(actions.registerLoading(false));
    } catch (error) {
        console.log(error.response)
    } finally {
        yield put(actions.registerLoading(false));
    }
}

function* verify(action) {
    try {
        yield put(actions.registerLoading(true));

        const { data } = yield call(AuthService.verify, action.payload);

        yield put(actions.registerLoading(false));

        yield put({ type: types.NEXT_STEP })
    } catch (error) {
        let errorMessage = null;
        // Parse if errors is an object
        if(error.response.data.errors) {
            errorMessage = error.response.data.errors;
        } else {
            // Parse if error is a string
            errorMessage = error.response.data.error;
        }
        
        yield put(actions.registerFailed(errorMessage));
    }
}

function* register(action) {
    try {
        yield put(actions.registerLoading(true));
    
        const { data } = yield call(AuthService.register, action.payload);
        
        // SHOW VERIFICATION CODE IN ALERT
        // Alert.alert('Код подтверждения', data.verification_code.toString());
        // *******************************

        yield put(actions.registerSuccess(action.payload.phone_number));
        
        navigate('WizardScreen', {
            phoneNumber: action.payload.phone_number
        });
    } catch (error) {
        let errorMessage = null;
        // Parse if errors is an object
        if(error.response.data.errors) {
            errorMessage = error.response.data.errors;
        } else {
            // Parse if error is a string
            errorMessage = error.response.data.error;
        }
        
        yield put(actions.registerFailed(errorMessage));
    } finally {
        yield put(actions.registerLoading(false));
    }
}

export function* registerSaga() {
    yield all([
        watchRegister(),
        watchVerify(),
        watchCompleteRegister()
    ]);
}
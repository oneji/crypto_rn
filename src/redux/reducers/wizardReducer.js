import * as types from '../constants/actionTypes'

const initialState = {
    step: 0
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.NEXT_STEP:
            return {
                ...state,
                step: state.step + 1
            }

        case types.RESET_STEPS:
            return {
                ...state,
                step: 0
            }
    
        default:
            return state;
    }
}
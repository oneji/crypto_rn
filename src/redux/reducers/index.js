import { combineReducers } from 'redux'
import registerReducer from './registerReducer'
import loginReducer from './loginReducer'
import wizardReducer from './wizardReducer'

export default combineReducers({
    register: registerReducer,
    wizard: wizardReducer,
    user: loginReducer
});
import * as types from '../constants/actionTypes'

const initialState = {
    loading: false,
    error: null,
    phone_number: '',
    email: ''
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.REGISTER_LOADING:
            return {
                ...state,
                loading: action.payload
            }
        case types.REGISTER_SUCCESS:
            return {
                ...state,
                loading: false,
                phone_number: action.payload,
                error: null
            }
        case types.REGISTER_FAILED:
            return {
                ...state,
                loading: false,
                error: action.payload,
            }
        case types.COMPLETE_REGISTER:
            return state;
        case types.SET_EMAIL:
            return {
                ...state,
                email: action.payload
            }
    
        default:
            return state;
    }
}
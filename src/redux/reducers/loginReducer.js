import * as types from '../constants/actionTypes'

const initialState = {
    loading: false,
    user: {},
    isAuthenticated: false,
    error: null,
    token: ''
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.LOGIN_LOADING:
            return {
                ...state,
                loading: action.payload
            }
        case types.LOGIN_SUCCESS:
            return {
                ...state,
                isAuthenticated: true,
                token: action.payload.token,
                user: action.payload.user,
            }
        case types.LOGIN_FAILED:
            return {
                ...state,
                user: null,
                isAuthenticated: false,
                error: action.payload
            }
        case types.LOGOUT:
            return {
                ...state, 
                isAuthenticated: false,
                user: {},
                token: null
            }
    
        default:
            return state;
    }
}
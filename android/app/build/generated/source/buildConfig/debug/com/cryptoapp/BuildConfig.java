/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.cryptoapp;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.cryptoapp";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from default config.
  public static final String API_URL = "http://testapi.na4u.ru/api";
}
